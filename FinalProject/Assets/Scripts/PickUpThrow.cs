﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpThrow : MonoBehaviour // the video is now private but I got this from a video via YouTube
{
    public Transform ObjectHolder;

    public float ThrowForce;

    public bool carryObject;
    public bool IsThrowable;

    public GameObject Item;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            Ray directionRay = new Ray(transform.position, transform.forward);
            if(Physics.Raycast(directionRay, out hit, 2f))
            {
                if(hit.collider.tag == "Object")
                {
                    carryObject = true;
                    IsThrowable = true;
                    if(carryObject == true)
                    {
                        Item = hit.collider.gameObject;
                        Item.transform.SetParent(ObjectHolder);
                        Item.gameObject.transform.position = ObjectHolder.position;
                        Item.GetComponent<Rigidbody>().isKinematic = true;
                        Item.GetComponent<Rigidbody>().useGravity = false;
                    }
                }
            }
        }
        
        if(Input.GetMouseButton(1)) // right mouse button
        {
            carryObject = false;
            IsThrowable = false;
        }

        if(carryObject == false)
        {
            ObjectHolder.DetachChildren();
            Item.GetComponent<Rigidbody>().isKinematic = false;
            Item.GetComponent<Rigidbody>().useGravity = true;
        }

        if(Input.GetMouseButton(0)) // left mouse button
        {
            if(IsThrowable)
            {
                ObjectHolder.DetachChildren();
                Item.GetComponent<Rigidbody>().isKinematic = false;
                Item.GetComponent<Rigidbody>().useGravity = true;
                Item.GetComponent<Rigidbody>().AddRelativeForce(transform.forward * ThrowForce);
            }
        }
    }
}
