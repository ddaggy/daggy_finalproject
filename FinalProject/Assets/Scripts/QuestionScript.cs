﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionScript : MonoBehaviour
{
    // public TMPro.TextMeshPro tmp;
    public TextMesh tmp;

    public void WrongAnswer()
    {
        tmp.color = Color.red;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        tmp.color = Color.Lerp(tmp.color, Color.white, 2f * Time.deltaTime); // lerp = blend between two values
    }
}
