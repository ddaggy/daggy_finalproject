﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer : MonoBehaviour
{
    public bool isCorrect;
    public GameObject CorrectQuestion;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Okay");
        if (other.gameObject.CompareTag("Question"))
        {
            if (other.gameObject == CorrectQuestion)
            {
                Debug.Log("Yes");
                other.gameObject.SetActive(false);
                gameObject.SetActive(false);
                Debug.Log("Yes again");
            }
            else
            {
                Debug.Log("Wrong");
                other.gameObject.GetComponent<QuestionScript>().WrongAnswer();
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
